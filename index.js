const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const { client } = require('./DB'); 
const app = express();

const PORT = 8080;
client.connect();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json())
app.use(cors());

app.post('/save', async (req, res) => {
  try {
    const { body } = req;
    console.log(body);
    if (!body.url) {
      throw new Error('Invalid request');
    }
    const slug = `${Date.now()}ABC`;
    const searchQuery = `
      select * from url_shortner where value = $1
    `
    const { rows } = client.query(searchQuery, [body.url])
    if (rows && rows.length > 0) {
      const objToReturn = {
        ...rows[0],
        exists: true,
      }
      return res.json(objToReturn);
    }
    const query = `
      insert into url_shortner(slug, value) values($1, $2)
    `;
    await client.query(query, [slug, body.url])
    return res.json({ slug });
  } catch(e) {
    console.log('Error', e)
    return res.send(400);
  }
})

app.get('/:slug', async (req, res) => {
  try { 
    const { params } = req;
    if (!params || !params.slug) {
      throw new Error('Invalid request');
    }
    const query = `
      select * from url_shortner where slug = $1
    `;
    const { rows } = await client.query(query, [params.slug]);
    if (!rows || rows.length === 0) {
      throw new Error('Not found');
    }
    const obj = rows[0];
    return res.json({
      redirectUrl: obj.value
    });
  } catch (e) {
    console.log('Error', e)
    return res.send(400);
  }
})

app.listen(PORT, () => { 
  console.log(`Server running at http://localhost:${PORT}`)
});