const { Client } = require('pg');
const DB_URL = 'postgres://peoplegrove:password@localhost:5432/pg_master'

const client = new Client({
  connectionString: DB_URL,
});

module.exports = {
  client
}