Instruction

- Node version 8.15
- npm install
- need to put DB_URL in DB.js
- node index.js

Create table in Your DB using

```
create table url_shortner(
    id serial PRIMARY key,
    slug text not null,
    value text not null,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
)
```

